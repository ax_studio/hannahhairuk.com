<?php

function create_posttype()
{

    register_post_type('Gallery',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Gallery'),
                'singular_name' => __('Gallery')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'gallery'),
            'menu_icon' => 'dashicons-format-gallery',
            'menu_position' => 1,
            'supports' => array('title', 'editor', 'author', 'excerpt', 'thumbnail', 'revisions', 'custom-fields','page-attributes'),

        )
    );

}

// Hooking up our function to theme setup
add_action('init', 'create_posttype');