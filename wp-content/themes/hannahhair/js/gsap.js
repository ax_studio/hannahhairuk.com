gsap.registerPlugin(ScrollTrigger);

// Heading animation
gsap.utils.toArray(".appear").forEach(heading => {
    var tl = gsap.timeline({
        scrollTrigger: {
            trigger: heading,
            toggleActions: "restart pause resume reset",
            start: "top 100%"
        }
    });

    tl.fromTo(heading, {
            opacity: 0,
            y: 30
        },
        {
            duration: 0.5,
            opacity: 1,
            y: 0,
            delay: 0.3
        });
});
