

function serviceAccordion() {
    $('.eael-accordion-header').addClass('active, show');
}


function menuToggle() {
    $('.header-toggle_menu').click(function() {
        $(this).toggleClass('active');
        if($(this).hasClass('active')) {
            $('.header-navigation').fadeIn();
        } else {
            $('.header-navigation').fadeOut();
        }


        if (document.documentElement.clientWidth < 1025) {
            $('.header-navigation ul li a').click(function() {
                $('.header-navigation').slideUp(700);
                $('.header-toggle_menu').removeClass('active');

            });
        }
    });

}

// function loadPage() {
//
//     $( window ).load(function() {
//         $('.loader').removeClass('move').delay(2000).addClass('move');
//     });
//
//     $( window ).beforeunload(function() {
//             $('.loader').removeClass('move');
//     });
//
//     // window.addEventListener('load', (event) => {
//     //     $('.loader').removeClass('move').delay(2000).addClass('move');
//     // });
//     //
//     // window.addEventListener('beforeunload', function(event) {
//     //     $('.loader').removeClass('move');
//     // });
//     // window.addEventListener('unload', function(event) {
//     //     // $('.loader').removeClass('move');
//     // });
// }


function scrollSpy() {
    $('.header-navigation ul li a').addClass('nav-link');
    $('.elementor-section-wrap').attr('data-spy','scroll');
    $('.elementor-section-wrap').attr('data-target','#spy');

    var section = document.querySelectorAll(".scrollhere");
    var sections = {};

    Array.prototype.forEach.call(section, function(e) {
        sections[e.id] = e.offsetTop;
    });



}

function backgroundZoom() {}


function accordionSettings() {
    $('.eael-accordion-list .eael-accordion-header').addClass('active');
    $('.eael-accordion-list .eael-accordion-header').addClass('show');
}


function textareaAutoGrow() {


    $('textarea').attr('rows', '1');

    $('textarea').each(function () {
        this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
    }).on('input', function () {
        this.style.height = 'auto';
        this.style.height = (this.scrollHeight) + 'px';
    });
}

function onScroll() {
    $(window).scroll(function () {

        if ($(this).scrollTop() > 150) {
            $('.scroll-button').fadeOut();
            $('.header').addClass('changeheader');


        } else {
            $('.scroll-button').fadeIn();
            $('.header').removeClass('changeheader').delay(2000);

        }
        if (document.documentElement.clientWidth < 755) {

            if ($(this).scrollTop() > 150) {
                $('.header-branding').addClass('visible');

            } else {
                $('.header-branding').removeClass('visible')
            }
        }



    });
}


function scrollToTop() {
    $('.scroll-up').click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
}

function onClickGoTo() {
    $(".scroll-button").click(function() {
        $('html,body').animate({
                scrollTop: $("#about").offset().top},
            'slow');
    });
}

function clock() {
    ({
        init: function() {
            var t = this;
            setInterval(function() {
                t.runClock()
            }, 1e3)        },
        runClock: function() {
            var t = new Date
                , e = t.getDay()
                , r = t.getHours()
                , i = t.getMinutes()
                , n = t.getSeconds()
                , o = t.getMilliseconds()
                , s = document.querySelector("div.clock")
                , a = s.querySelector("div.hour")
                , u = s.querySelector("div.minute")
                , l = s.querySelector("div.second")
                , h = r % 12 * 30 + .5 * i
                , c = 6 * i + .1 * n
                , f = 6 * n + .006 * o
                , d = 6 === e
                , p = 0 === e
                , v = $(".footer .opening-hours")
                , g = v.data("weekdaysOpen")
                , m = v.data("weekdaysClosed")
                , y = v.data("saturdayOpen")
                , _ = v.data("saturdayClosed")
                , x = $(".footer .opening-hours span")
                , b = x.data("open")
                , T = x.data("closed");
            a.style.transform = "rotate(" + h + "deg)",
                u.style.transform = "rotate(" + c + "deg)",
                l.style.transform = "rotate(" + f + "deg)",
                p ? x.text(T) : d ? r >= y && i >= 0 && r < _ ? x.text(b) : x.text(T) : r >= g && i >= 0 && r < m ? x.text(b) : x.text(T)
        }
    }).init()
}
