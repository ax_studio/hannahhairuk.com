<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162970190-10"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-162970190-10');
    </script>


    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target="#spy">
<!--<div class="loader">-->
<!--    <div class="loader_texts">-->
<!--        <div><p>Loading</p></div>-->
<!--        <div><p>Hannah Hair Uk</p></div>-->
<!--    </div>-->
<!--</div>-->
<div class="site">

    <header id="header" class="header">
        <div class="header-branding">
            <?php the_custom_logo(); ?>
            <a class="header-branding_red">
                <img src="<?php echo site_url(); ?>/wp-content/uploads/2020/09/hannahhairuk-worksans-red2.png" />
            </a>
        </div>

        <nav class="header-navigation" id="spy">
            <div class="header-navigation_container">
                <?php if (is_front_page() ) : ?>
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'primary',
                    'menu_id' => 'header-menu',
                    'menu_class' => 'header-menu',
                ));
                ?>
                <?php else : ?>

                <?php
                wp_nav_menu(array(
                    'theme_location' => 'default'
                ));
                ?>

                <?php endif; ?>
            </div>
        </nav>
        <div class="header-toggle">
            <div class="header-toggle_phone">
                <a href="tel:07392494146"><i class="las la-phone"></i></a>
            </div>
            <div class="header-toggle_menu"><span class="menu-cross"></span></div>
        </div>
    </header>

    <div class="site-content">
