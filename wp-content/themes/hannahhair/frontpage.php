<?php
/*
Template Name: Front Page
*/

get_header();
?>
    <main class="frontpage">


        <div class="frontpage_background bg bg-fixed" style="background-image: url('<?php echo site_url(); ?>/wp-content/uploads/2020/09/225.jpg'); background-position: center; background-size: cover;">

        </div>

        <?php
        while (have_posts()) :
            the_post();

            the_content();


        endwhile;
        ?>
    </main>


<?php
get_footer();

