<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AX_studio
 */

?>

</div>

<footer class="footer">

    <div>
        <a class="btn btn-white" href="tel:07392494146">Book Now ― 07392 494146</a>
    </div>
</footer>
</div>

<?php wp_footer(); ?>

</body>
</html>
