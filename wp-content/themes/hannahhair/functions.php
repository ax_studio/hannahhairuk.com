<?php
/**
 * AX studio functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AX_studio
 */

if ( ! defined( '_S_VERSION' ) ) {
    // Replace the version number of the theme on each release.
    define( '_S_VERSION', '2.0.0' );
}

if ( ! function_exists( 'axstudio_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function axstudio_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on AX studio, use a find and replace
		 * to change 'axstudio' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'axstudio', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'axstudio' ),
			'default' => esc_html__( 'Default', 'axstudio' ),
			'header-second' => esc_html__( 'Secondary', 'axstudio' ),
			'footer' => esc_html__( 'Footer', 'axstudio' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'axstudio_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

        add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );

    }
endif;
add_action( 'after_setup_theme', 'axstudio_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function axstudio_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'axstudio_content_width', 640 );
}
add_action( 'after_setup_theme', 'axstudio_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function axstudio_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'axstudio' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'axstudio' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'axstudio_widgets_init' );

///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
// Enqueue scripts and styles.
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//

function axstudio_scripts() {
	wp_enqueue_style( 'axstudio-style', get_stylesheet_uri() );

    wp_enqueue_style('lineawesome', 'https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.1.0/css/line-awesome.min.css');

    wp_enqueue_script( 'gsap', 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/gsap-latest-beta.min.js', array(), '20151215', true );

    wp_enqueue_script( 'ScrollTrigger', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollTrigger.min.js', array(), '20151215', true );


    wp_enqueue_script( 'gsap',  'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.4.2/gsap.min.js', array(), '20151215', true );
    
    wp_enqueue_script( 'datatables',  'https://cdn.datatables.net/v/bs4/dt-1.10.21/af-2.3.5/datatables.min.js', array(), '20151215', true );

    wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js', array(), '20151215', true );


    wp_enqueue_script( 'function', get_template_directory_uri() . '/js/function.js', array(), '20151215', true );

    wp_enqueue_script( 'app', get_template_directory_uri() . '/js/app.js', array(), '20151215', true );

    wp_enqueue_script( 'gsap-function', get_template_directory_uri() . '/js/gsap.js', array(), '20151215', true );



    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'axstudio_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}



show_admin_bar(false);



///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
// POST TYPE FUNCTIONS
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
require 'functions/post-type.php';

///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
// CUSTOM LOGIN PAGE
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
require 'functions/custom-login.php';

///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
// Excerpts
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
require 'functions/excerpts.php';

///XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
// ADD ROLE
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX//
require 'functions/add-role.php';



